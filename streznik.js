var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});


app.get('/api/seznam', function(req, res) {
	res.send(noviceSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje novic)
 */
app.get('/api/dodaj', function(req, res) {
  // ...
  //res.send('Potrebno je implementirati dodajanje novice!');
  // ...
  
  var naslov = req.query.naslov;
  var maxID = 0;
  for (var i in noviceSpomin) {
    if (noviceSpomin[i].id > maxID) maxID = noviceSpomin[i].id;
  }
  maxID++;
  
  var povzetek = req.query.povzetek;
  var kategorija = req.query.kategorija;
  var postnaStevilka = req.query.postnaStevilka;
  var kraj = req.query.kraj;
  var povezava = req.query.povezava;
  
  noviceSpomin.push({
				naslov: naslov, 
        id: maxID,
        povzetek: povzetek,
        kategorija: kategorija,
        postnaStevilka: postnaStevilka,
        kraj: kraj,
        povezava: povezava
			});
			res.redirect('/');
});



/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje novice)
 */
app.get('/api/brisi', function(req, res) {
  // ...
  //res.send('Potrebno je implementirati brisanje novice!');
  // ...
  
  //console.log(res);
  //console.log("IDDD" + req.query.id);
  var tmp = req.query.id;
  //console.log(tmp);

  if (tmp != undefined) {
    console.log("brisanje v teku");
    //res.send("OK");
    var obstaja = false;
    for (var i in noviceSpomin) {
      if (noviceSpomin[i].id == tmp) {
        noviceSpomin.splice(i, 1);
				obstaja = true;
      }
    }
    if (obstaja == true) {
      console.log("pobrisano");
      res.status(200);
      res.redirect('/');
    }
    else res.send("Novica z id-jem <id> ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>"); 
  }
  else {
    console.log("napaka!")
    //res.send("Novica z id-jem <id> ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>"); 
  }
  
  
  
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var noviceSpomin = [
  {
    id: 1,
    naslov: 'Slovenija in korupcija: končali smo v družbi Mehike in Kolumbije',
    povzetek: 'Slovenija krši mednarodne zaveze v boju proti podkupovanju, opozarjajo pri slovenski podružnici TI. Konvencijo o boju proti podkupovanju tujih javnih uslužbencev v mednarodnem poslovanju OECD namreč izvajamo "malo ali nič".',
    kategorija: 'novice',
    postnaStevilka: 1000,
    kraj: 'Ljubljana',
    povezava: 'http://www.24ur.com/novice/slovenija/slovenija-in-korupcija-koncali-smo-v-druzbi-mehike-in-kolumbije.html'
  }, {
    id: 2,
    naslov: 'V Postojni udaren začetek festivala z ognjenim srcem',
    povzetek: 'V Postojni se je z nastopom glasbenega kolektiva The Stroj začel tradicionalni dvotedenski festival Zmaj ma mlade.',
    kategorija: 'zabava',
    postnaStevilka: 6230,
    kraj: 'Postojna',
    povezava: 'http://www.rtvslo.si/zabava/druzabna-kronika/v-postojni-udaren-zacetek-festivala-z-ognjenim-srcem/372125'
  }
];
